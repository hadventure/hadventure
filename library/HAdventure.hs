{-# LANGUAGE TemplateHaskell #-}
-- | An example module.
module HAdventure (main) where

import Control.Lens
import Data.List

data Room = Room {
    _longDescription :: String,
    _shortDescription :: String,
    _exits :: [Room]
}

makeLenses ''Room

emptyRoom :: Room
emptyRoom = Room {_longDescription = "", _shortDescription = "", _exits = []}

(-->) :: Room -> Room -> Room
(-->) r c = exits .~ (c : (r^.exits)) $ r

describe :: Room -> String -> Room
describe r s = longDescription .~ s $ r

shortDescribe :: Room -> String -> Room
shortDescribe r s = shortDescription .~ s $ r

stupid_describe :: Room -> String -> String
stupid_describe r _ = r^.longDescription ++ "\nExits are " ++ (listExits r)

listExits :: Room -> String
listExits r = intercalate ", " $ map (view shortDescription) (r^.exits)

testRoom :: Room
testRoom = makeRoom "Not very busy bus station" "You are at a kind of busy bus station. It smells of cigarette smoke and cheap perfume." [testRoom2, testRoom3]

makeRoom :: String -> String -> [Room] -> Room
makeRoom short long es = foldl (-->) ((shortDescription .~ short) . (longDescription .~ long) $ emptyRoom) es

testRoom2 :: Room 
testRoom2 = shortDescribe (describe emptyRoom "asdegfaewfaea") "asdf"

testRoom3 :: Room
testRoom3 = shortDescription .~ "ghjk" $ testRoom2

-- | An example function.
main :: IO ()
main = mainloop

mainloop :: IO ()
mainloop = do
    putStr ">"
    l <- getLine
    putStrLn $ stupid_describe testRoom l
    mainloop